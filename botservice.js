const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const config = require('./configdb.js');
const bodyParser = require('body-parser');

router.use(bodyParser.text({ type: 'application/json' }));

//GET Service Online
router.get('/', function (req, res) {

    var response = {success: true, msg:'online'};
    res.json(response);

});

//GET: Read de bot information from database
router.get('/:id', function (req, res) {

    ReadBotByID(req.params.id, function (error, result){

        if (error) {
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not found with error: ' +  error.message});
            return;
        }

        if(result.length == 0 || result.length > 1)
        {
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not found with id: ' +  req.params.id});
        }

        res.json(result[0]);

    });
});

//PUT: Create new bot in the database
router.put('/', function (req, res) {

    var botInfo = JSON.parse(req.body);

    ReadBotByID(botInfo.id, function (error, result){
        
        if(error != null){
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not created: ' +  error});
            return;
        }

        if(result.length > 0){
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot already exists'});
            return;
        }

        CreateBot(botInfo.id, botInfo.name, function (error, result){

            if(error != null){
                res.statusCode = 400;
                res.json({ success: false, errors: 'bot not created: ' +  error});
                return;
            }

            res.json({ success: true, errors: 'bot created successfully'});
        });
    }); 
});

//DELETE
router.delete('/:id', function (req, res) {

    RemoveBot(req.params.id, function (error, result){
        if (error) {
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not deleted with error: ' +  error.message});
            return;
        }

        if(result.affectedRows == 1){
            res.json({ success: true, errors: 'bot removed successfully'});
        }
        else{
            res.json({ success: false, errors: 'bot not removed'});
        }       

    });

});

//POST: UPDATE bot in the database
router.post('/', function (req, res) {

    var botInfo = JSON.parse(req.body);

    //Validation
    if(botInfo.id == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'bot id required'});
    }

    if(botInfo.name == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'bot name required'});
    }

    ReadBotByID(botInfo.id, function (error, result){
        
        if(error != null){
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not updated: ' +  error});
            return;
        }

        if(result.length == 0){
            res.statusCode = 400;
            res.json({ success: false, errors: 'bot not exists'});
            return;
        }

        UpdateBot(botInfo.id, botInfo.name, function (error, result){

            if(error != null){
                res.statusCode = 400;
                res.json({ success: false, errors: 'bot not updated: ' +  error});
                return;
            }

            res.json({ success: true, errors: 'bot updated successfully'});
        });
    });

});

//READ info in database
function ReadBotByID(botId, callback) {

    let connection = mysql.createConnection(config);

    let sql = "SELECT * FROM bot WHERE id = '" + botId + "';"

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to search bot: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

//CREATE a new bot in database
function CreateBot(botId, botName, callback) {

    let connection = mysql.createConnection(config);

    let sql = "INSERT INTO bot VALUES ('" + botId + "'," + "'" +  botName + "');"

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to create bot: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

//DELETE bot in database
function RemoveBot(botId, callback) {

    let connection = mysql.createConnection(config);

    let sql = "DELETE FROM bot WHERE id = '" + botId + "';"

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to delete bot: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

//UPDATE a bot in database
function UpdateBot(botId, botName, callback) {

    let connection = mysql.createConnection(config);

    let sql = "UPDATE bot SET name = '" + botName + "' WHERE id = '" +  botId + "';"

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to update bot: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}


module.exports = router;