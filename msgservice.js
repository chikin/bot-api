const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const config = require('./configdb.js');
const bodyParser = require('body-parser');
const moment = require('moment');

router.use(bodyParser.text({ type: 'application/json' }));

//POST: register msg in database
router.post('/', function (req, res) {

    var botInfo = JSON.parse(req.body);

    //Validation
    if(botInfo.conversationId == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'conversationId required'});
    }

    if(botInfo.from == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'from required'});
    }

    if(botInfo.to == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'to required'});
    }

    if(botInfo.text == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'text required'});
    }

    SaveMessage(botInfo, function (error, result){
        
        if(error != null){
            res.statusCode = 400;
            res.json({ success: false, errors: 'message not created: ' +  error});
            return;
        }

        if(result.affectedRows == 1){
            res.json({ success: true, errors: 'message created successfully'});
        }
        else{
            res.json({ success: false, errors: 'message not created'});
        } 

    });

});

//GET: get msg in database
router.get('/:id', function (req, res) {

    ReadMsgByID(req.params.id, function (error, result){

        if (error) {
            res.statusCode = 400;
            res.json({ success: false, errors: 'msg not found with error: ' +  error.message});
            return;
        }

        if(result.length == 0 || result.length > 1)
        {
            res.statusCode = 400;
            res.json({ success: false, errors: 'msg not found with id: ' +  req.params.id});
        }

        res.json(result[0]);

    });
});

//GET: get msg in database
router.get('/', function (req, res) {

    var conversationId = req.query.conversationId;

    res.setHeader('Content-Type', 'application/json');

    if(conversationId == undefined){
        res.statusCode = 400;
        res.json({ success: false, errors: 'conversationId required'});
    }

    ReadMsgByConversationId(conversationId, function (error, result){

        if (error) {
            res.statusCode = 400;
            res.json({ success: false, errors: 'msg not found with error: ' +  error.message});
            return;
        }

        if(result.length == 0)
        {
            res.statusCode = 400;
            res.json({ success: false, errors: 'msg not found with conversationId: ' +  conversationId});
            return;
        }

        res.json(result);

    });
});


//CREATE new message in database
function SaveMessage(msg, callback) {

    let connection = mysql.createConnection(config);

    var mysqlTimestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');

    let sql = "INSERT INTO messages (messages.conversationId, messages.timestamp, messages.from, messages.to, messages.text) VALUES ('" 
              + msg.conversationId + "'," + "'" 
              + mysqlTimestamp + "'," + "'"
              + msg.from + "'," + "'"  
              + msg.to + "'," + "'" 
              + msg.text + "');";

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to create message: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

//READ info in database
function ReadMsgByID(msgId, callback) {

    let connection = mysql.createConnection(config);

    let sql = "SELECT * FROM messages WHERE id = '" + msgId + "';"

    connection.query(sql, true, (error, results, fields) => {
        if (error) {
            callback('error to search msg: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

//READ info in database by conversation
function ReadMsgByConversationId(conversationId, callback) {

    let connection = mysql.createConnection(config);

    let sql = "SELECT * FROM messages WHERE conversationId = '" + conversationId + "';"

    connection.query(sql, true, (error, results) => {
        if (error) {
            callback('error to return messages: ' +  error.message, null);
            return;
        }

        callback(null, results);     
    });

    connection.end();
}

module.exports = router;