const express = require('express')
const app = express()
const botservice = require('./botservice');
const msgservice = require('./msgservice');

app.use('/bots', botservice);
app.use('/messages', msgservice);

//INICIA O SERVIDOR
app.listen(process.env.PORT || 3000, function () {
    console.log('Servidor iniciado para bot API');
})